(async ()=>{
	"use strict";

	const fs		= require( 'fs' );
	const pemu 		= require( 'paraemu' );
	const levelkv 	= require( 'levelkv' );
	const logger 	= require( 'eggglogger').CreateLogger({isDebug:false, writeFile:"record.txt", isNeedTimeStamp:true});

	const protocol = Object.freeze({
		I_AM_COLLECTOR: "IamCollector",
		I_AM_ANALYSIS: "IamAnalysis",
		// ANALYSIS_NOTIFY_REQ: "analysis-notify-req",
		// ANALYSIS_NOTIFY_ACK: "analysis-notify-ack",
		ANALYSIS_NOTIFY_START: "analysis-notify-start",
		ANALYSIS_SEND_NODE_START: "analysis-send-node-start",
		ANALYSIS_NODE: "analysis-node",
		ANALYSIS_SEND_NODE_END: "analysis-send-node-end",
		ANALYSIS_NOTIFY_END: "analysis-notify-end",
	});
	const workFlow = Object.freeze({
		// TASKS_READY: "tasks-ready",
		WORK_DOWN: "workDone",
	});

	const LEVELKV_DB_PATH = `./analysisDB`;
	const PROGREE_INTERVAL = 60000;

	let { parent:g_parent, id:g_threadId } = pemu.args;// pemu.args = workerData
	let g_leafs 	= {};
	let g_nodes		= {};
	let g_blockTree	= {};
	let g_packData	= {};
	let g_levelDB	= null;
	let g_progress  = {};

	try{
		pemu
		.on(protocol.ANALYSIS_NOTIFY_START, async (e, {batchId})=>{
			logger.Log(`  [analysis][thread_${g_threadId}]--ANALYSIS_NOTIFY_START batchId(${batchId}) `);
			// 使用 levelKV
			if( !fs.existsSync(LEVELKV_DB_PATH) ){
				fs.mkdirSync(LEVELKV_DB_PATH);
			}
			if( !fs.existsSync(LEVELKV_DB_PATH+`/${batchId}`) ){
				fs.mkdirSync(LEVELKV_DB_PATH+`/${batchId}`);
			}
			g_levelDB = await levelkv.initFromPath(LEVELKV_DB_PATH+`/${batchId}`);
			if( e.respondWith ) e.respondWith('');
		})
		.on(protocol.ANALYSIS_SEND_NODE_START, (e, {batchId, nodeId})=>{
			// logger.Log(`  [analysis][thread_${g_threadId}]--ANALYSIS_SEND_NODE_START batchId(${batchId}) nodeId(${nodeId})`);
			let packIndex = `${batchId}-${nodeId}`;
			g_packData[packIndex] = "";
		})
		.on(protocol.ANALYSIS_NODE, (e, {batchId, nodeId, data})=>{
			// logger.Log(`  [analysis][thread_${g_threadId}]--ANALYSIS_NODE batchId(${batchId}) nodeId(${nodeId}) data(${data})`);
			let packIndex = `${batchId}-${nodeId}`;
			g_packData[packIndex] = g_packData[packIndex].concat(data);
		})
		.on(protocol.ANALYSIS_SEND_NODE_END, async (e, {batchId, nodeId})=>{
			// logger.Log(`  [analysis][thread_${g_threadId}]--ANALYSIS_SEND_NODE_END batchId(${batchId}) nodeId(${nodeId})`);
			let packIndex = `${batchId}-${nodeId}`;
			let node = JSON.parse( g_packData[packIndex] );
			await g_levelDB.put( node.id, node );
			delete g_packData[packIndex];
		})
		.on(protocol.ANALYSIS_NOTIFY_END, async (e, {batchId})=>{
			logger.Log(`  [analysis][thread_${g_threadId}]--ANALYSIS_NOTIFY_END batchId(${batchId}) `);

			let t1 = await __STATISTICS();
			logger.Info(`  [analysis][thread_${g_threadId}]batchId(${batchId})===> __STATISTICS() cost ${t1}(ms)`);

			let leafArray = [];
			let t2 = await __BUILD_TREE(leafArray);
			logger.Info(`  [analysis][thread_${g_threadId}]batchId(${batchId})===> __BUILD_TREE() cost ${t2}(ms)`);

			let result = {};
			let leafNum = leafArray.length;
			let t3 = await __ANALYSIS(leafArray, result);
			logger.Info(`  [analysis][thread_${g_threadId}]batchId(${batchId})===> __ANALYSIS() cost ${t3}(ms), ${leafNum} leaf`);
			logger.Info(`  [analysis][thread_${g_threadId}]batchId(${batchId})===> finalScore(${result.score}), ${t1+t2+t3}(ms) ${result.memory}(MB)`);

			pemu.send(g_parent, workFlow.WORK_DOWN, batchId, result.score);

			await g_levelDB.close();
			g_levelDB = null;
		})
	}catch(e){
		logger.Error(`  [analysis][process] exception`);
		logger.Error(e);
		throw new Error(e);
	}

	async function __STATISTICS(){
		let timeStart = Date.now();
		let levelDBCursor = await g_levelDB.get();
		___PROGRESS_START("__STATISTICS", levelDBCursor.size, PROGREE_INTERVAL);
		for await (let node of levelDBCursor){
			___PROGRESS_ADD("__STATISTICS");
			let addNodesNum = false;
			let blockHash 	= node.latest;
			if( !g_nodes.hasOwnProperty(node.id) && blockHash ){
				g_nodes[node.id] = blockHash;
				addNodesNum = true;
				// logger.Log(`    [thread]batchId(${batchId})nodeHash(${nodeHash}) latest(${blockHash})`);
			}

			// 檢查 leaf 有無存在
			let leaf = g_leafs.hasOwnProperty(blockHash) ? g_leafs[blockHash] : {hash:blockHash, nodesNum:0, blocks:{}};
			leaf.nodesNum = addNodesNum ? leaf.nodesNum+1 : leaf.nodesNum;
			let blocks = node.blocks;
			for(let b in blocks){
				if( !leaf.blocks.hasOwnProperty(b) ){
					leaf.blocks[b] = blocks[b].parent;
				}
			}
			g_leafs[blockHash] = leaf;
		}
		___PROGRESS_END("__STATISTICS");
		return Date.now() - timeStart;
	}
	function __BUILD_TREE(out_leafArray=[]){
		let timeStart = Date.now();
		let i = 0;
		___PROGRESS_START("__BUILD_TREE", Object.keys(g_leafs).length, PROGREE_INTERVAL);
		for(let lh in g_leafs){
			___PROGRESS_ADD("__BUILD_TREE");
			// g_leafs.blocks 轉換成 array
			let leaf = g_leafs[lh];
			leaf.index = i++;
			let pathArray = [];
			let hash = leaf.hash;

			// logger.Log(leaf);
			while( hash !== "0000000000000000000000000000000000000000000000000000" ){
				pathArray.push(hash);
				hash = leaf.blocks[hash];
			}
			pathArray = pathArray.reverse();
			leaf.blocks = pathArray.slice();

			// build tree
			let blockTreeIterator = g_blockTree;
			let leafId = "";
			while( pathArray.length > 0 ){
				hash = pathArray.shift();
				let value = null;
				// logger.Info(hash);
				if( !blockTreeIterator.hasOwnProperty(hash) ){
					let copyV = blockTreeIterator.v;
					delete blockTreeIterator.v;

					// case1: 都空的
					if( Object.keys(blockTreeIterator).length === 0 ){ value = 1; }

					// case2: 有東西
					else{
						value = 0;
						for(let h in Object.keys(blockTreeIterator)){
							if( hash === h ){
								value = blockTreeIterator[hash].v;
								break;
							}
						}
					}
					blockTreeIterator.v = copyV;
					blockTreeIterator[hash] = {v:value};
				}else{
					value = blockTreeIterator[hash].v;
				}

				leafId += value;
				blockTreeIterator = blockTreeIterator[hash];
			}
			// logger.Log(`    [thread]batchId(${batchId})L_${leaf.index} build tree, ${Date.now()-time1}(ms)`);
			leaf.id = leafId;

			// 再把 g_leafs 轉換成 array
			out_leafArray.push(leaf)
		}
		___PROGRESS_END("__BUILD_TREE");
		g_leafs = null;
		return Date.now() - timeStart;
	}
	async function __ANALYSIS(leafArray, out_result={}){
			let score = 0;
			let denominator = 0;
			let timeStart = Date.now();
			// logger.Log(`    [thread]總共 ${leafArray.length} leaf`);
			___PROGRESS_START("__ANALYSIS", leafArray.length, PROGREE_INTERVAL);
			while( leafArray.length > 0 ){
				___PROGRESS_ADD("__ANALYSIS");
				let timeStart2 = Date.now();
				let leaf = leafArray.shift();
				let myLength = leaf.id.length;

				// step 2.1: 自己配對
				let score1 = 0
				if( leaf.nodesNum > 1 ){
					score1 = ___COMBINATORICS_BY_2(leaf.nodesNum);
					denominator += score1;
				}

				// step 2.2: 和別人配對
				let score2 = 0;
				for(let l of leafArray){
					let otherLength = l.id.length;
					let maxPath = Math.max(myLength, otherLength);
					let maxSamePath = await ___DIVIDE_AND_CONQUER(leaf.id, l.id);
					score2 += maxSamePath*leaf.nodesNum*l.nodesNum/maxPath;
					denominator += leaf.nodesNum*l.nodesNum;
				}
				logger.Log(`    [thread](L_${leaf.index}, ${leaf.nodesNum}) score1(${score1}) score2(${score2}) , ${Date.now()-timeStart2}(ms)`);
				score += score1 + score2;
			}
			___PROGRESS_END("__ANALYSIS");
			out_result.score = Math.round(1000*score/denominator)/1000;
			out_result.memory = Math.round((process.memoryUsage().rss)/1000)/1000;
			return Date.now() - timeStart;
		}

	function ___DIVIDE_AND_CONQUER(Astring, Bstring, cut=null, lastCompare=null){
		// console.log(`hi`);
		return new Promise((resolve)=>{
			setTimeout(async ()=>{
				let h = Math.min( Astring.length, Bstring.length );
				let nowLeft = lastCompare ? lastCompare.left : 0;
				let nowRight = lastCompare ? lastCompare.right : h+1;

				cut = cut ? cut : Math.floor( h/2 );
				// console.log(`${runTimes++} cut(${cut})`);
				let tmpAstring = Astring.slice(0, cut);
				let tmpBstring = Bstring.slice(0, cut);
				let result = Buffer.compare( Buffer.from(tmpAstring), Buffer.from(tmpBstring) ) === 0;
				// console.log(`${runTimes++} cut(${cut}) result(${result})`);

				let newCut 		= null;
				let lastCut 	= lastCompare ? lastCompare.lastCut : null;
				let lastResult 	= lastCompare ? lastCompare.lastResult : null;

				if( result ){
					nowLeft = cut;
					newCut = Math.floor((cut+nowRight)/2);
					newCut = newCut === cut ? newCut+1 : newCut;
					if( newCut >= nowRight ){
						resolve(cut);
						return;
					}
				}
				else{
					nowRight = cut;
					newCut = Math.floor((cut+nowLeft)/2);
					newCut = newCut === cut ? newCut-1 : newCut;
					if( newCut === nowLeft ){
						if( lastCompare && lastResult ){
							resolve(lastCut);
							return;
						}
						resolve(0);
						return;
					}
				}

				let r = await ___DIVIDE_AND_CONQUER(Astring, Bstring, newCut, {lastCut:cut, lastResult:result, left:nowLeft, right:nowRight});
				resolve(r);
				return;
			}, 0);
		});
	}
	function ___COMBINATORICS_BY_2(m){
		if( m < 2 ){
			throw new Error(`combinatorics Error: m(${m}) is less than n(2)`);
		}else if( m === 2 ) return 1;

		return (m-1)*m/2;
	}
	async function ___COMBINATORICS(m, n){
		if( m < n ){
			throw new Error(`combinatorics Error: m(${m}) is less than n(${n})`);
		}else if( n <= 0 ){
			throw new Error(`combinatorics Error: n(${n}) is less than 0`);
		}
		if( m === n ) return 1;
		let numerator = await ___FACTORIAL(m);
		let denominator_1 = await ___FACTORIAL(n);
		let denominator_2 = await ___FACTORIAL(m-n);
		return numerator/denominator_1/denominator_2;
	}
	function ___FACTORIAL(n){
		return new Promise((resolve)=>{
			setTimeout(async ()=>{
				if( n === 1 ){
					resolve(1);
					return;
				}
				let r = await ___FACTORIAL(n-1);
				resolve(n*r);
				return;
			}, 0);
		});
	}
	function ___PROGRESS_START(key, total, time=600000){
		g_progress[key] = {total, count:0, timer:null};
		logger.Info(`  [analysis][thread_${g_threadId}]${key} ${g_progress[key].count}/${g_progress[key].total} (${Math.round(g_progress[key].count*100000/g_progress[key].total)/1000}%)`);
		let t = setInterval(()=>{
			logger.Info(`  [analysis][thread_${g_threadId}]${key} ${g_progress[key].count}/${g_progress[key].total} (${Math.round(g_progress[key].count*100000/g_progress[key].total)/1000}%)`);
		}, time)// 10 min
		g_progress[key].timer = t;
	}
	function ___PROGRESS_ADD(key, add=1){
		g_progress[key].count += add;
	}
	function ___PROGRESS_END(key){
		logger.Info(`  [analysis][thread_${g_threadId}]${key} ${g_progress[key].count}/${g_progress[key].total} (${Math.round(g_progress[key].count*100000/g_progress[key].total)/1000}%)`);
		clearInterval(g_progress[key].timer);
		delete g_progress[key];
	}
})();