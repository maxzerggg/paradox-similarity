(()=>{
	"use strict";

	const fs		= require( 'fs' );
	const pemu 		= require( 'paraemu' );
	const levelkv 	= require( 'levelkv' );
	const logger 	= require( 'eggglogger').CreateLogger({isDebug:false, writeFile:"record.txt", isNeedTimeStamp:true});

	const protocol = Object.freeze({
		I_AM_COLLECTOR: "IamCollector",
		I_AM_ANALYSIS: "IamAnalysis",
		ANALYSIS_NOTIFY_REQ: "analysis-notify-req",
		ANALYSIS_NOTIFY_ACK: "analysis-notify-ack",
		ANALYSIS_NOTIFY_START: "analysis-notify-start",
		ANALYSIS_SEND_NODE_START: "analysis-send-node-start",
		ANALYSIS_NODE: "analysis-node",
		ANALYSIS_SEND_NODE_END: "analysis-send-node-end",
		ANALYSIS_NOTIFY_END: "analysis-notify-end",
	});
	const workFlow = Object.freeze({
		TASKS_READY: "tasks-ready",
		WORK_DOWN: "workDone",
	});

	const MAX_THREAD = 4;
	const LEVELKV_DB_PATH = `./packDB`;

	let g_levelDB	 = {};
	let g_idleThread = [];
	let g_workThread = {};
	let g_collector	 = null;
	let g_packData	 = {};


	try{
		pemu
		.on(workFlow.TASKS_READY, async (e)=>{
			logger.Log(`  [analysis][process]--tasks-ready`);

			// 建立 thread
			for(let i=0; i<MAX_THREAD; i++){
				let thread = await pemu.job("../100_RUN/thread.js", {workerData:{parent:pemu.uniqueId, id:i}});
				g_idleThread.push(thread.uniqueId);
				logger.Log(`  [analysis][process]create thread_${i}`);
			}

			pemu.emit(protocol.I_AM_ANALYSIS, pemu.uniqueId);// 回報給controller
		})
		.on(protocol.I_AM_COLLECTOR, (e, analysisId)=>{
			logger.Log(`  [analysis][process]--I_AM_COLLECTOR  ${analysisId}`);
			g_collector = analysisId;
		})
		.on(protocol.ANALYSIS_NOTIFY_REQ, async (e, {batchId})=>{
			logger.Log(`  [analysis][process]--ANALYSIS_NOTIFY_REQ batchId(${batchId})`);

			let handler = pemu.uniqueId;
			if( g_idleThread.length > 0 ){
				handler = g_idleThread.shift();
				g_workThread[batchId] = { threadId:handler, timeStart:null };
				try{
					await pemu.deliver(handler, protocol.ANALYSIS_NOTIFY_START, {batchId})//NOTICE: 提早送訊息給thread建立levelKV
				}catch(e){// 訊息沒發送成功 30秒後pemu.deliver會丟出reject
					logger.Error(e);
					throw e;
				}
			}
			else{
				logger.Info(`  [analysis][process]pack ${batchId}`);
				// 使用 levelKV
				if( !fs.existsSync(LEVELKV_DB_PATH) ){
					fs.mkdirSync(LEVELKV_DB_PATH);
				}
				if( !fs.existsSync(LEVELKV_DB_PATH+`/${batchId}`) ){
					fs.mkdirSync(LEVELKV_DB_PATH+`/${batchId}`);
				}
				g_levelDB[batchId] = await levelkv.initFromPath(LEVELKV_DB_PATH+`/${batchId}`);
			}
			pemu.send(g_collector, protocol.ANALYSIS_NOTIFY_ACK, {handler});
		})
		.on(protocol.ANALYSIS_NOTIFY_START, (e, {batchId})=>{
			logger.Log(`  [analysis][process]--ANALYSIS_NOTIFY_START batchId(${batchId}) `);
			let threadOrder = g_workThread[batchId];
			if( threadOrder ){
				threadOrder.timeStart = Date.now();
			}
		})
		.on(protocol.ANALYSIS_SEND_NODE_START, (e, {batchId, nodeId})=>{
			logger.Log(`  [analysis][process]--ANALYSIS_SEND_NODE_START batchId(${batchId}) nodeId(${nodeId})`);
			let packIndex = `${batchId}-${nodeId}`;
			g_packData[packIndex] = "";
		})
		.on(protocol.ANALYSIS_NODE, (e, {batchId, nodeId, data})=>{
			logger.Log(`  [analysis][process]--ANALYSIS_NODE batchId(${batchId}) nodeId(${nodeId})`);
			let packIndex = `${batchId}-${nodeId}`;
			g_packData[packIndex] = g_packData[packIndex].concat(data);
		})
		.on(protocol.ANALYSIS_SEND_NODE_END, async (e, {batchId, nodeId})=>{
			logger.Log(`  [analysis][process]--ANALYSIS_SEND_NODE_END batchId(${batchId}) nodeId(${nodeId})`);
			let packIndex = `${batchId}-${nodeId}`;
			let node = JSON.parse( g_packData[packIndex] );

			await g_levelDB[batchId].put( node.id, node );
			delete g_packData[packIndex];
		})
		.on(protocol.ANALYSIS_NOTIFY_END, async (e, {batchId})=>{
			logger.Log(`  [analysis][process]--ANALYSIS_NOTIFY_END batchId(${batchId}) `);

			// NOTICE: 牽扯到nodejs底層 C控制區塊, 所以故意延遲一下再關閉
			setTimeout(async ()=>{
				await g_levelDB[batchId].close();
				delete g_levelDB[batchId];
			}, 1000);

		})
		.on(workFlow.WORK_DOWN, (e, batchId, data)=>{
			logger.Log(`  [analysis][process]--workDone batchId(${batchId}) `);
			let threadId = e.sender;
			let costTime = Date.now() - g_workThread[batchId].timeStart;
			logger.Info(`  [analysis][process]batchId${batchId} work finished, score(${data}), ${costTime}(ms)`);
			delete g_workThread[batchId];
			g_idleThread.push(threadId);
		})
	}catch(e){
		logger.Error(`  [analysis][process] exception`);
		logger.Error(e);
		throw new Error(e);
	}
})();